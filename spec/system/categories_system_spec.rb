require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :system do
  describe "categories_system_spec_test" do
    let(:taxonomy) { create(:taxonomy, name: "Brand") }
    let(:taxon) { create(:taxon, name: "Rails", parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let(:product) { create(:product, name: "RUBY_ON_RAILS_BAG", price: 22, taxons: [taxon]) }
    let(:image) { create(:image) }
    let(:fake_taxon) { create(:taxon, name: "@", parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let(:fake_product) { create(:product, name: "TEST_PRODUCT", price: 19, taxon: [fake_taxon]) }

    before do
      product.master.images = [image]
      visit potepan_category_path(taxon.id)
    end

    describe "Scenario testing for category pages" do
      scenario "It's showing the data associated with the category" do
        expect(page).to have_selector ".page-title", text: "Rails"
        expect(page).to have_selector ".breadcrumb", text: "Rails"
        expect(page).to have_selector ".productCaption", text: "$22"
        expect(page).to have_selector ".productCaption", text: "RUBY_ON_RAILS_BAG"
      end

      scenario "Data associated with other categories are not displayed" do
        expect(page).not_to have_selector ".page-title", text: "@"
        expect(page).not_to have_selector ".breadcrumb", text: "@"
        expect(page).not_to have_selector ".productCaption", text: "$19"
        expect(page).not_to have_selector ".productCaption", text: "TEST_PRODUCT"
      end
    end

    describe "Testing the link to top_page" do
      it "Test the Logo's link" do
        click_link "Logo_image"
        expect(current_path).to eq potepan_path
      end

      it "Test the Header's link" do
        within '.navbar-right' do
          click_link "HOME"
        end
        expect(current_path).to eq potepan_path
      end

      it "Test the LIGHT-SECTION's link" do
        within '.breadcrumb' do
          click_link "HOME"
        end
        expect(current_path).to eq potepan_path
      end
    end

    describe "Testing the category page display" do
      it "'productBox' is displayed with the number of products" do
        expect(page).to have_selector('.productBox', count: taxon.all_products.count)
      end

      it "Product links are implemented correctly." do
        click_link "RUBY_ON_RAILS_BAG"
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end

    describe "Testing product images" do
      it "Product images are displayed correctly" do
        expect(page).to have_selector("img[alt=商品画像-#{product.id}]")
      end

      it "Product image links are implemented correctly" do
        click_link "商品画像-#{product.id}"
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end

    describe "Testing the sidebar" do
      it "Appropriate categories are displayed in the sidebar" do
        within ".side-nav" do
          expect(page).to have_content "Brand"
          expect(page).to have_content "Rails (1)"
        end
      end

      it "Test the sidebar's link" do
        click_link "#{taxon.name}"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end
  end
end
