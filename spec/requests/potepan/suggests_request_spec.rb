require 'rails_helper'
require 'webmock/rspec'

RSpec.describe "Potepan::Suggests", type: :request do
  describe "GET /suggest" do
    let(:api_url) { ENV["API_URL"] }
    let(:api_key) { ENV['API_KEY'] }

    before do
      WebMock.enable!
      WebMock.stub_request(:get, ENV["API_URL"]).
        with(
          headers: header,
          query: { keyword: keyword, max_num: 5 }
        ).to_return(
          body: return_body,
          status: return_status
        )
      get potepan_suggests_path params: { keyword: keyword, max_num: 5 }
    end

    context "If the correct query is issued." do
      let(:header)        { { Authorization: "Bearer #{ENV['API_KEY']}", "Content-Type": "application/json" } }
      let(:keyword)       { "ruby" }
      let(:return_body)   { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json }
      let(:return_status) { 200 }

      it "status:200 is being returned." do
        expect(response.status).to eq 200
      end

      it "The correct array elements will be returned." do
        expect(response.body).to eq ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json
      end
    end

    context "If there is something wrong with the query that was issued." do
      let(:header)        { { Authorization: "Bearer #{ENV['API_KEY']}", "Content-Type": "application/json" } }
      let(:keyword)       { " " }
      let(:return_body)   { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json }
      let(:return_status) { 400 }

      it "status:400 is being returned." do
        expect(response.status).to eq 400
      end
    end

    context "If there is a problem on the server side." do
      let(:header)        { { Authorization: "Bearer #{ENV['API_KEY']}", "Content-Type": "application/json" } }
      let(:keyword)       { "ruby" }
      let(:return_body)   { "Server Error" }
      let(:return_status) { 500 }

      it "status:500 is being returned." do
        expect(response.status).to eq 500
      end
    end
  end
end
