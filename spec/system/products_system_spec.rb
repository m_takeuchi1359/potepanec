require 'rails_helper'

RSpec.describe "Potepan::Products", type: :system do
  describe "products_system_spec_test" do
    let(:taxonomy) { create(:taxonomy, name: "Brand") }
    let(:taxon) { create(:taxon, name: "Rails", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:product) { create(:product, name: "R", price: 22, description: "ABC", taxons: [taxon]) }
    let(:f_taxon) { create(:taxon, name: "@", parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let!(:f_product) { create(:product, name: "F", price: 4, description: "DE", taxons: [f_taxon]) }
    let!(:related_product) { create(:product, name: "related_product", taxons: [taxon]) }
    let!(:related_product_max) { Settings.decorator.related_product_max }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    describe "Scenario testing of product pages" do
      scenario "information about the selected product will be displayed." do
        expect(page).to have_selector ".page-title", text: product.name
        expect(page).to have_selector ".breadcrumb", text: product.name
        expect(page).to have_selector ".media-body", text: product.name
        expect(page).to have_selector ".media-body", text: product.price
        expect(page).to have_selector ".media-body", text: product.description
      end

      scenario "Information for products that are not selected will not be displayed" do
        expect(page).not_to have_selector ".page-title", text: f_product.name
        expect(page).not_to have_selector ".breadcrumb", text: f_product.name
        expect(page).not_to have_selector ".media-body", text: f_product.name
        expect(page).not_to have_selector ".media-body", text: f_product.price
        expect(page).not_to have_selector ".media-body", text: f_product.description
      end

      scenario "The correct related products are displayed for the selected product." do
        expect(page).to have_selector ".productCaption", text: related_product.name
        expect(page).to have_selector ".productCaption", text: related_product.display_price
      end
    end

    describe "Testing the link to Top_page" do
      it "Test the Logo's link" do
        click_link "Logo_image"
        expect(current_path).to eq potepan_path
      end

      it "Test the Header's link" do
        within ".navbar-right" do
          click_link "HOME"
        end
        expect(current_path).to eq potepan_path
      end

      it "Test the LIGHT-SECTION's link" do
        within ".breadcrumb" do
          click_link "HOME"
        end
        expect(current_path).to eq potepan_path
      end
    end

    describe "Testing the link to category_page" do
      it "Test the Category_page's link" do
        click_link "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    describe "Testing related product blocks" do
      it "Related product images will be displayed correctly" do
        expect(page).to have_selector("img[alt=関連商品画像-#{related_product.id}]")
      end

      it "Related Product image links are implemented correctly" do
        within ".productsContent" do
          click_link related_product.name
        end
        expect(current_path).to include(potepan_product_path(related_product.id))
      end

      it "The maximum number of related products to be displayed is 4" do
        expect(page).to have_selector ".productBox", count: 4
      end
    end
  end
end
