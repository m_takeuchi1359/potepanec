require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "helper_spec_test" do
    context "If the page title exist" do
      it "The 'full_title' is displayed" do
        expect(full_title("title")).to eq "title | BIGBAG"
      end
    end

    context "If the page title doesn’t exist" do
      it "Only BIGBAG is shown." do
        expect(full_title("")).to eq "BIGBAG"
      end
    end

    context "If 'nil' is given as the page title argument" do
      it "Only BIGBAG is shown." do
        expect(full_title(nil)).to eq "BIGBAG"
      end
    end
  end
end
