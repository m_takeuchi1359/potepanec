require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /category" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon)    { create(:taxon, name: "Ruby", parent_id: taxonomy.root.id, taxonomy: taxonomy) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "HTTP status 200 is returned" do
      expect(response).to have_http_status(200)
    end

    it "The product category (taxonomy) is displayed" do
      expect(response.body).to include taxonomy.name
    end

    it "The product classification (taxon) is displayed" do
      expect(response.body).to include taxon.name
    end

    it "The number of products is displayed against the product category (taxon)" do
      expect(response.body).to include taxon.products.count.to_s
    end
  end
end
