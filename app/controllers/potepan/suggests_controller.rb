require "./lib/api/suggest.rb"

class Potepan::SuggestsController < ApplicationController
  def search
    if params[:keyword].blank?
      logger.error "status: 400 keyword is blank"
      return render status: 400, json: { message: "Request error 'keyword is blank'" }
    end

    response = Api::Suggest.suggest(params[:keyword], params[:max_num])

    if response.status == 200
      render status: 200, json: JSON.parse(response.body)
    else
      logger.error "#{response.status} server error"
      render status: response.status, json: { message: "#{response.status} server error" }
    end
  end
end
