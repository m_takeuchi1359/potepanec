require 'httpclient'

class Api::Suggest
  def self.suggest(keyword, max_num)
    url = ENV['API_URL']
    header = { Authorization: "Bearer #{ENV['API_KEY']}", "Content-Type": "application/json" }
    query = { keyword: keyword, max_num: max_num }
    client = HTTPClient.new
    client.get(url, header: header, query: query)
  end
end
