require "rails_helper"

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /product" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon)    { create(:taxon, name: "Ruby", parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let(:product)  { create(:product, name: "R", price: 19, description: "ABC", taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "HTTP status 200 is returned" do
      expect(response).to have_http_status(200)
    end

    it "The product name is displayed" do
      expect(response.body).to include product.name
    end

    it "The price of the product is displayed" do
      expect(response.body).to include product.display_price.to_s
    end

    it "The product description should be displayed" do
      expect(response.body).to include product.description
    end
  end
end
