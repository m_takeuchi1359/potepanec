require 'rails_helper'

RSpec.describe "Spree::Product", type: :model do
  describe "products_model_spec_test" do
    let(:taxonomy) { create(:taxonomy, name: "Brand") }
    let(:taxon) { create(:taxon, name: "Rails", parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let(:product)  { create(:product, name: "R", taxons: [taxon, f_taxon]) }
    let(:products) { product.add_related_products }
    let(:f_taxon) { create(:taxon, name: "@", parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let!(:f_product) { create(:product, name: "f_product", taxons: [f_taxon]) }
    let!(:related_product) { create(:product, name: "related_product", taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    describe "Product data acquisition test" do
      it "getting related products" do
        expect(products[1].name).to eq related_product.name
      end

      it "The product itself is not acquired" do
        expect(products).not_to include product.id
      end

      it "No acquisition except for related products" do
        expect(products).not_to include f_product.id
      end

      it "No duplication of related products" do
        expect(related_product.add_related_products).to match_array(
          [related_products[0], related_products[1], related_products[2], related_products[3]]
        )
      end
    end
  end
end
