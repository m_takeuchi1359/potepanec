Capybara.server = :puma, { Silent: true }
Capybara.javascript_driver = :webkit
Capybara.default_driver = :selenium

Capybara.register_driver :chrome_headless do |app|
  options = Selenium::WebDriver::Chrome::Options.new
  options.add_argument('--headless')
  options.add_argument('--disable-gpu')
  # driver = Selenium::WebDriver.for :chrome
end

RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  config.before(:each, type: :system, js: true) do
    driven_by :selenium_chrome_headless
  end
end
