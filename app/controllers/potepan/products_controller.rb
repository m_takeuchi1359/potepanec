class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.add_related_products.
      limit(Settings.decorator.related_product_max).
      includes(master: [:images, :default_price])
  end
end
